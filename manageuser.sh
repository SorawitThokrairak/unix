#!/bin/bash

filenow=$(date +"%Y%m%d%T")
namefile="user-$filenow"

add () {
read -p "Please enter username :" username
read -p "Please enter Fullname :" fullname
read -p "Select Student / Lecture [S/L] :" select
pwd=$(pwgen -s1 10)
now=$(date +"%Y-%m-%d %T")
checkselect=`echo $select | tr [:lower:] [:upper:]`
if [ $checkselect == "S" ]; then
   useradd $username -c "$fullname" -d /home/student/$username -g student
   echo "==========================================================" >> "$namefile".txt
   echo "Username: $username     Fullname: $fullname" >> "$namefile".txt
   echo "Password: $pwd          Home: /home/student/$username" >> "$namefile".txt
   echo "Last Modify: $now" >> "$namefile".txt
else
   useradd $username -c "$fullname" -d /home/lecture/$username -g lecture
   echo "==========================================================" >> "$namefile".txt
   echo "Username: $username     Fullname: $fullname" >> "$namefile".txt
   echo "Password: $pwd          Home: /home/lecture/$username" >> "$namefile".txt
   echo "Last Modify: $now" >> "$namefile".txt
fi
}

chpwd () {
 read -p "Please enter username:" pwdusername
 pwd=$(pwgen -s1 10)
 echo "==========================================================" >> "$namefile".txt
 echo "Username: $pwdusername	Password: $pwd" >> "$namefile".txt
}

deluser () {
 read -p "Please enter username:" delusername
 read -p "Account [username] is delete. Are you sure? [Y/N]" delsure
 checkdelsure=`echo $delsure | tr [:lower:] [:upper:]`
 if [ $checkdelsure == "Y" ]; then
    userdel $delusername -r
 fi
}

let replay=0
until [ $replay -eq 1 ]
do
   echo "=========================================================="
   echo "User Management for CentOS"
   echo "=========================================================="
   echo "	1.Add User"
   echo "	2.Change Password"
   echo "	3.Delete User"
   echo "	4.Exit"
read -p "Please select [1-4]:" state
if [ $state -eq 1 ]; then
   add
elif [ $state -eq 2 ]; then
   chpwd
elif [ $state -eq 3 ]; then
   deluser
else
   let replay=1
fi
done

exit 0;
